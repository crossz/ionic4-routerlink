## Router In Ionic 4

This is from the Ionic blog, https://ionicframework.com/blog/navigating-the-change-with-ionic-4-and-angular-router/.

Here are the codes for this simple demo.


Furthermore, more tests on the router properties and specific features, such as "skip a page in browser history when navigating back", which is listed here https://stackoverflow.com/questions/41874826/how-to-skip-a-page-in-browser-history-when-navigating-back, are demonstrated in this small project.


### Expriment 1: skip a page in browser history

This is useful when we do a hybrid app, users should NOT be able to 'go back' in any way from the HOME/ROOT/FIRST page.

This is demonstrated in the app/pages/login. There are slightly modification for removing a history path when navigating back to dashboard in the html and ts files.